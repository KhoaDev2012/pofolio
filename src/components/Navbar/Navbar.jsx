import logo from "../../assets/kevinRushLogo.png";
import { FaSquareXTwitter } from "react-icons/fa6";
import { FaLinkedin, FaGithub, FaInstagram } from "react-icons/fa";

export default function Navbar() {
  return (
    <nav className=" flex items-center justify-between mb-20 py-6">
      {/* LOGO */}
      <div className="flex flex-shrink-0 items-center">
        <img src={logo} alt="" className="mx-4 w-10" />
      </div>
      {/* HEADER ICONS */}
      <div className="m-8 flex items-center justify-center gap-4 text-2xl">
        <FaLinkedin />
        <FaGithub />
        <FaInstagram />
        <FaSquareXTwitter />
      </div>
    </nav>
  );
}
